print('colorclock v0.1')

-- variable initialisation
timestring = nil
hours = 0
minutes = 0
seconds = 0

-- init ws2812 module
ws2812.init()
leds = ws2812.newBuffer(60, 3)

-- load config
if file.exists("config.lua") then
    require("config")
    config = true
else
    print("Could not load config. Abort!")
    leds:fill(0,20,0)
    ws2812.write(leds)
end

-- setup wifi
wifi.setmode(wifi.STATION)
wifi.sta.config(wifiname,wifipassword)

-- wait for wifi
tmr.alarm(0, 1000, 1, function()
    if wifi.sta.getip() == nil then
       print("Connecting to AP...")
   else
      print('IP: ',wifi.sta.getip())
      tmr.stop(0)
   end
end)

function gettime()
    -- do nothing if wifi not connected
    if wifi.sta.getip() == nil then
        return
    end
    time=nil
    conn=net.createConnection(net.TCP, 0) 
    conn:on("connection",function(conn, payload)
    conn:send("GET /"..timezone.."/now "..
        "HTTP/1.1\r\n".. 
        "Host: www.timeapi.org\r\n"..
        "Accept: */*\r\n"..
        "User-Agent: Mozilla/4.0 (compatible; esp8266 Lua;)"..
        "\r\n\r\n") 
    end)
            
    conn:on("receive", function(conn, payload)
        for line in string.gmatch(payload.."\n", "([^\n]*)\n") do
            datetime = line
        end;
        print('DateTime: '..datetime)
        conn:close()
        timestring = datetime
        hours = tonumber(string.sub(timestring,12,13))
        minutes = tonumber(string.sub(timestring,15,16))
        seconds = tonumber(string.sub(timestring,18,19))
    end) 
    conn:connect(80,'www.timeapi.org')
end

tmr.alarm(1, 5000, 1, function()
	if wifi.sta.getip() == nil then
		print("Not connected to WiFi. Trying again later...")
	else 
		gettime()
		tmr.stop(1)
  	end
end)

function updateLeds()
	if timestring ~= nil then
		print("current time: "..hours..":"..minutes..":"..seconds)
		leds:fill(0,0,0)
		leds:set(seconds+1, string.char(0, 250, 0))
		leds:set(minutes+1, string.char(250, 0, 0))
		if hours > 12 then
			hoursled = (hours-12)*5+minutes/12
		else
			hoursled = hours*5+minutes/12
		end
		leds:set(hoursled+1,0,0,250)
		if seconds == minutes then
			leds:set(seconds+1, 250,250,0)
		end
		if seconds ==  hoursled then
			leds:set(seconds+1, 0,250,250)
		end
		if minutes == hoursled then
			leds:set(minutes+1, 250,0,250)
		end
		if seconds == minutes and minutes == hoursled then
			leds:set(seconds+1, 250,250,250)
		end
		ws2812.write(leds)
        seconds = seconds + 1
        if seconds == 60 then
            seconds = 0
            minutes = minutes + 1 
            if minutes == 60 then
                minutes = 0
                hours = hours + 1
                if hours == 24 then
                    hours = 0
                end
            end
        end
	end
end

tmr.alarm(2, 1000, tmr.ALARM_AUTO, function () updateLeds() end)
tmr.alarm(3, 3600000, tmr.ALARM_AUTO, function() gettime() end)


